﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using UnmanagedMemoryLib;

using System.Runtime.InteropServices;

namespace Test
{
    class Program
    {
        static unsafe void Main(string[] args)
        {
            Console.WriteLine(string.Format("pointer size in bytes: {0}", sizeof(void*)));

            //var hHeap = HeapApi.HeapCreate(HeapApi.Flags.HEAP_GENERATE_EXCEPTIONS, UIntPtr.Zero, UIntPtr.Zero);
            var hHeap = HeapApi.GetProcessHeap();
            Debug.Assert(IntPtr.Zero != hHeap);
            //ulong size = 0x80000000; // 2GB
            ulong size = 0x40000000; // 1GB
            //ulong size = 62;
            Console.WriteLine("allocating");
            var lpMem = HeapApi.HeapAlloc(hHeap, 0/*HeapApi.Flags.HEAP_ZERO_MEMORY*/, new UIntPtr(size));
            Debug.Assert(null != lpMem);
            var allocated_size = HeapApi.HeapSize(hHeap, 0, lpMem).ToUInt64();
            Debug.Assert(allocated_size != ulong.MaxValue);
            Console.WriteLine("allocated delta: {0}", allocated_size - size);
            Debug.Assert(HeapApi.HeapValidate(hHeap, 0, lpMem));

            Console.WriteLine("writing...");

            ulong bytesWritten = 0;

            byte foo;

            // write
            byte* start = (byte*)lpMem.ToPointer();
            byte* end = start + size;
            foo = 0;
            for (byte* b = start; b != end; ++b)
            {
                *b = foo;
                //Console.WriteLine(*b);
                foo = (byte)(foo + 1);
                ++bytesWritten;
            }

            Debug.Assert(HeapApi.HeapValidate(hHeap, 0, lpMem));

            Console.WriteLine(bytesWritten);

            HeapApi.HeapAlloc(hHeap, 0/*HeapApi.Flags.HEAP_ZERO_MEMORY*/, new UIntPtr(1024));


            //Console.WriteLine("checking...");

            // check
            //foo = 0;
            //for (var b = lpMem; b != (lpMem + size); b++)
            //{
            //    //Debug.Assert(*b == foo);
            //    //byte c = *b;
            //    //Console.WriteLine(*b);
            //    Debug.Assert(0 == *b);
            //    //*b = foo;
            //    foo = (byte)(foo + 1);
            //}

            Console.ReadKey();

            Console.WriteLine("Destroying heap");

            //bool success = HeapApi.HeapDestroy(HeapApi.GetProcessHeap());
            if (hHeap != HeapApi.GetProcessHeap())
            {
                bool success = HeapApi.HeapDestroy(hHeap);
                if (false == success)
                {
                    Console.WriteLine("last error: {0}", Marshal.GetLastWin32Error());
                }
                Debug.Assert(success);
            }

            Console.WriteLine("done");

            Console.ReadKey();
        }
    }
}
