﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace UnmanagedMemoryLib
{
    /// <summary>
    /// http://msdn.microsoft.com/en-us/library/aa366711.aspx
    /// </summary>
    public static class HeapApi
    {
        /// <summary>
        /// 
        /// </summary>
        [Flags]
        public enum Flags : uint
        {
            /// <summary>
            /// Heap must not be accessed by multiple threads at the same time.
            /// </summary>
            HEAP_NO_SERIALIZE = 0x00000001,

            /// <summary>
            /// On error, HeapAlloc and HeapReAlloc throw System.OutOfMemoryException or System.AccessViolationException instead of returning a zero pointer.
            /// </summary>
            HEAP_GENERATE_EXCEPTIONS = 0x00000004,

            /// <summary>
            /// Allocated memory is initialized to zero.
            /// </summary>
            HEAP_ZERO_MEMORY = 0x00000008,

            /// <summary>
            /// HeapReAlloc shall not move memory blocks.
            /// </summary>
            HEAP_REALLOC_IN_PLACE_ONLY = 0x00000010,

            /// <summary>
            /// Allocated memory can be executed.
            /// </summary>
            HEAP_CREATE_ENABLE_EXECUTE = 0x00040000,
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr GetProcessHeap();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flOptions"></param>
        /// <param name="dwInitialSize"></param>
        /// <param name="dwMaximumSize"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr HeapCreate(Flags flOptions, UIntPtr dwInitialSize, UIntPtr dwMaximumSize);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hHeap"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool HeapDestroy(IntPtr hHeap);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hHeap">Handle to heap</param>
        /// <param name="dwFlags"></param>
        /// <param name="dwBytes"></param>
        /// <returns>
        /// Error: Zero.
        /// Success: Start address of allocated block that is AT LEAST as big as requested.
        /// </returns>
        [DllImport("kernel32.dll", SetLastError = false)]
        public static extern UIntPtr HeapAlloc(IntPtr hHeap, Flags dwFlags, UIntPtr dwBytes);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hHeap"></param>
        /// <param name="dwFlags"></param>
        /// <param name="lpMem"></param>
        /// <param name="dwBytes"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = false)]
        public static extern UIntPtr HeapReAlloc(IntPtr hHeap, Flags dwFlags, UIntPtr lpMem, UIntPtr dwBytes);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hHeap"></param>
        /// <param name="dwFlags"></param>
        /// <param name="lpMem"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool HeapFree(IntPtr hHeap, Flags dwFlags, UIntPtr lpMem);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hHeap"></param>
        /// <param name="dwFlags"></param>
        /// <param name="lpMem"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = false)]
        public static extern UIntPtr HeapSize(IntPtr hHeap, Flags dwFlags, UIntPtr lpMem);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hHeap"></param>
        /// <param name="dwFlags"></param>
        /// <param name="lpMem"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = false)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool HeapValidate(IntPtr hHeap, Flags dwFlags, UIntPtr lpMem);
    }
}
